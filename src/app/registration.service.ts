import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { of } from 'rxjs/observable/of';

import { catchError, tap, map } from 'rxjs/Operators';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';




import { Registration } from './models/registration-model';



const httpOptions = {
  headers: new HttpHeaders({'content-type': 'application/json'})
};

@Injectable()
export class RegistrationService {

  private registrationUrl: string = 'http://localhost:8000/registrations/';

  constructor( private _http: Http, private _httpClient: HttpClient) { }

  addRegistration(registration: Registration){

    return new Promise((resolve, reject)=> {
      let headers = new Headers();
      headers.append('Content-Type', 'application/json');

      this._http.post(this.registrationUrl, JSON.stringify(registration),{headers: headers})
      .subscribe(res => {
        resolve(res.json());
      }, (err) => {
        reject(err);
      });
    });
  }


    getRegistrations(){
      return this._http.get(this.registrationUrl).toPromise().then((res)=>{
          console.log(res);
      });

    }



}
