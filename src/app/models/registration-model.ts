

export class Registration {

  public first_name: string ;
  public last_name: string;
  public addr: string;
  public house_num: number;
  public city : string;
  public plz: number;
  public phone_number: number ;
  public email: string ;

  /* first occupation attr */
  public first_reg_bool: boolean = false;



  public birth_date: Date;
  public birth_date_str: string;
  public birth_place: string ;
  public birth_name: string ;
  public nationality: string ;
  public marit_stat: string ;


  /*Id's*/
  public tax_id: string='' ;
  public social_id: string='' ;
  public insurance: string ;
  public insurance_loc: string ;


  /* Occupation Attr*/
  public occupation: string;
  public other_occupation_bool: boolean;
  public other_occupation: string;
  public main_occup_bool: boolean ;
  public main_occup: string;
  public beside_occup_bool: boolean ;
  public beside_occup: string;
  public beside_occup_income: number;


  constructor(


  ){}



}
