import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatButtonModule,
         MatToolbarModule,
         MatInputModule,
         MatProgressSpinnerModule,
         MatCardModule,
         MatExpansionModule,
         MatIconModule,
         MatFormFieldModule,
         MatRadioModule,
         MatDatepickerModule,
         MatNativeDateModule,
         MatDialogModule,
         
     } from '@angular/material';

@NgModule({
  imports: [MatButtonModule,
           MatToolbarModule,
           MatInputModule,
           MatProgressSpinnerModule,
           MatCardModule,
           MatExpansionModule,
           MatIconModule,
           MatFormFieldModule,
           MatRadioModule,
           MatDatepickerModule,
           MatNativeDateModule,
           MatDialogModule,

       ],

  exports: [MatButtonModule,
           MatToolbarModule,
           MatInputModule,
           MatProgressSpinnerModule,
           MatCardModule,
           MatExpansionModule,
           MatIconModule,
           MatFormFieldModule,
           MatRadioModule,
           MatDatepickerModule,
           MatNativeDateModule,
           MatDialogModule,

       ],
})
export class MaterialModule {}
