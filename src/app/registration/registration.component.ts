import { Component, OnInit, Inject } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl }  from '@angular/forms';
import { HttpClient } from  '@angular/common/http';

import { MaterialModule } from '../material.module';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { taxIdValidator,
         socialIdValidator,
        // firstRegValidator,
         resetValidator,
         occupValidator,
         resetValidatorOtherOccup,
         conditionalValidatorOccupation,
         conditionalValidatorMainOccupation,
         conditionalValidatorBesideOccupation} from '../validators/reg.validators';

import { Registration } from '../models/registration-model'
import { RegistrationService } from '../registration.service';



@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {

  post: any;
  step = 0;





  PersonalFormGroup: FormGroup;
  IdFormGroup: FormGroup;
  OccupationFormGroup: FormGroup;
  FirstRegFormGroup: FormGroup;
  InsuranceFormGroup: FormGroup;


  Registration: Registration;

  start_date= new Date(1995,0,1);

  file: any;


  constructor(private _formBuilder: FormBuilder, private _registrationService: RegistrationService,
              public successDialog: MatDialog,
              ) { }

  ngOnInit() {

    this.Registration=new Registration;
    this.PersonalFormGroup = this._formBuilder.group({
      firstNameCtrl:['',Validators.required],
      lastNameCtrl:['',Validators.required],
      addrCtrl:['',Validators.required],
      numCtrl:['',Validators.required],
      phoneCtrl:['',Validators.compose([Validators.required, Validators.pattern("[0-9]*")])],
      emailCtrl:['',Validators.compose([Validators.required, Validators.email])],
      cityCtrl:['',Validators.required],
      plzCtrl:['',Validators.compose([Validators.required, Validators.pattern("[0-9]*")])],
    });
    this.IdFormGroup = this._formBuilder.group({
      taxIdCtrl:['',taxIdValidator],
      socialIdCtrl:['',Validators.compose([socialIdValidator])],
      //firstRegCtrl:['',Validators.compose([Validators.required])],
      });
    this.FirstRegFormGroup = this._formBuilder.group({
      birthDateCtrl:['', Validators.required],
      birthPlaceCtrl:['', Validators.required],
      birthNameCtrl:['', Validators.required],
      nationalityCtrl:['', Validators.required],
      maritStatCtrl:['', Validators.required],
      genderCtrl:['', Validators.required],


    });
    this.InsuranceFormGroup = this._formBuilder.group({
      insuranceCtrl:['',Validators.compose([Validators.required])],
      insuranceLocCtrl:['',Validators.compose([Validators.required])],

    });
    this.OccupationFormGroup = this._formBuilder.group({
      occupCtrl:['',Validators.compose([resetValidator,occupValidator])],
      otherOccupCtrl:['',Validators.compose([resetValidatorOtherOccup,conditionalValidatorOccupation])],
      otherOccupStrCtrl:[''],
      mainOccupCtrl:['',Validators.compose([Validators.required, conditionalValidatorMainOccupation])],
      mainOccupStrCtrl:[''],
      besideOccupCtrl:['',Validators.compose([Validators.required, conditionalValidatorBesideOccupation])],
      besideOccupStrCtrl:[''],
      besideOccupIncomeCtrl:[''],



    });

  }

  submit(): void{
    if(!this.PersonalFormGroup.valid){
      this.setStep(0);
      return null;
    }
    if(!this.IdFormGroup.valid && this.Registration.first_reg_bool===false){
      this.setStep(1);
      return null;
    }
    if(!this.FirstRegFormGroup.valid && this.Registration.first_reg_bool===true){
      this.setStep(1);
      return null;
    }
    if(!this.InsuranceFormGroup.valid){
      this.setStep(2);
      return null;
    }
    if(!this.OccupationFormGroup.valid){
      this.setStep(3);
      return null;
    }

    if(this.Registration.birth_date){
          this.Registration.birth_date_str=this.dateFormatter(this.Registration.birth_date)
      }
      this._registrationService.addRegistration(this.Registration).then(res => this.showRegistrationSuccessModal());

    }

    showRegistrationSuccessModal(): void{
      let dialogRef = this.successDialog.open(RegistrationDialog, {
        width: '250px',
        data : this.Registration,
      });

    }


  setStep(index: number){
    this.step = index;
  }

  nextStep(formGroup){
    if(formGroup.valid){
        this.step++;
    }
    else{
      console.log("not valid")
    }

  }

  prevStep(){
    this.step--;
  }

  dateFormatter(date: Date):string {
    return date.getFullYear().toString()+"-"+date.getMonth().toString()+"-"+date.getDate().toString()

  }




}


//registration modal validation component:


@Component({
  selector: 'registration-dialog',
  templateUrl: 'registration-dialog.html'

})

export class RegistrationDialog {

  constructor(public dialogRef: MatDialogRef<RegistrationDialog>,
  @Inject(MAT_DIALOG_DATA) public data: any){}


}
