import { AbstractControl, Validators }from '@angular/forms';


export function taxIdValidator(control: AbstractControl){
  let taxId = control.value;
    if (taxId.length != 11){
      return {
        taxIdLength: {
          idLength: taxId.length
        }
      }
    }


  return null;
}



export function socialIdValidator(control: AbstractControl){

  let socId = control.value;
  var num_exp = new RegExp('^[0-9]+$','i')
  var alpha_exp = new RegExp('^[a-z]+$','i')


    if (socId.length != 12){
      return {
        socIdLength: {
          idLength: socId.length
        }
      }
    }
    let block_1=socId.slice(0,8);
    let block_2=socId.slice(8,9);
    let block_3=socId.slice(9,12);
    console.log(num_exp.test(block_1));
    if (!num_exp.test(block_1)){
        return {
          socIdblock_1 :{
            block_1: block_1
          }
        }
    }
    console.log(block_2);
    if (!alpha_exp.test(block_2)){
        return {
          socIdblock_2 :{
            block_2: block_2
          }
        }
    }
    if (!num_exp.test(block_3)){
        return {
          socIdblock_3 :{
            block_3: block_3
          }
        }
    }

    console.log(block_1)

    return null;
   }




export function resetValidator(control: AbstractControl){
  if(control.value){
    control.parent.get('otherOccupCtrl').setValue(false);
    return null;
  }


}

export function resetValidatorOtherOccup(control: AbstractControl){
  if(control.value){
    control.parent.get('occupCtrl').setValue('');
    return null;
  }

}


export function occupValidator(control: AbstractControl){

  if(control.parent){
    if(!control.value && !control.parent.get('otherOccupCtrl').value){
      return {
        required :{
          required : control.value
        }
      }
    }

  }



}


export function conditionalValidatorOccupation(control: AbstractControl){

      if(control.parent){
        if (control.value==true){
          control.parent.get('otherOccupStrCtrl').setValidators(Validators.required);
        }
        if(control.parent.get('otherOccupStrCtrl').hasError('required')){
          return {
            required :{
              required : control.value
            }
          }
        }

  }



}

export function conditionalValidatorMainOccupation(control: AbstractControl){


  if(control.parent){
  if (control.value==true){
    control.parent.get('mainOccupStrCtrl').setValidators(Validators.required);
  }
  if(control.parent.get('mainOccupStrCtrl').hasError('required')){
    return {
      required :{
        required : control.value
      }
    }
  }

}
}


export function conditionalValidatorBesideOccupation(control: AbstractControl){
if(control.parent){
if (control.value==true){
  control.parent.get('besideOccupStrCtrl').setValidators(Validators.required);
  control.parent.get('besideOccupIncomeCtrl').setValidators(Validators.required);

}
if(control.parent.get('besideOccupStrCtrl').hasError('required')){
  return {
    required :{
      required : control.value
    }
  }
}
if(control.parent.get('besideOccupIncomeCtrl').hasError('required')){
  return {
    required :{
      required : control.value
    }
  }
}

}




}
/*

export function firstRegValidator(control:AbstractControl){
if(control.parent){

  if(control.value==true){
    control.parent.get('taxIdCtrl').clearValidators();
    control.parent.get('socialIdCtrl').clearValidators();
    console.log(control.value)

    return null;
  }
  else{
    control.parent.get('taxIdCtrl').setValidators(Validators.compose([Validators.required, Validators.pattern("[0-9]*"),taxIdValidator]));
    control.parent.get('socialIdCtrl').setValidators(socialIdValidator);
    console.log(control.value)
    return null;





  }
}



}
*/
